const Discord = require('discord.js');
const _ = require('lodash');
const markdownTable = require('markdown-table');
const token = require('./token.js');
const jsonFile = require('jsonfile');
const GIS = require('g-i-s');

const allAvailableChamps = {};
const client = new Discord.Client();
jsonFile.readFile('lolChamps.json').then((obj) => {
  Object.assign(allAvailableChamps, obj.data);
});

const roles = ['ADC', 'Supp', 'Mid', 'Jung', 'Top'];

const roleClasses = {
  ADC: ['Marksman'],
  Supp: ['Support', 'Mage', 'Tank'],
  Mid: ['Mage', 'Assassin'],
  Jung: ['Fighter', 'Tank'],
  Top: ['Fighter', 'Tank', 'Assassin'],
};

const displayPlayer = function displayPlayer(player, flags) {
  if (typeof player === 'undefined') {
    return '';
  }
  const out = [];
  if (flags.role.active) {
    out.push(`Role: ${player.role}`);
  }
  if (flags.champ.active) {
    out.push(`Champ: ${player.champ}`);
  }

  return out.length ? [player.name, out.join(', ')].join(' - ') : player.name;
};

const getPlayers = function getPlayers(msg, addPlayers, delim) {
  const addSplitMsgArr = msg.content.split(delim);
  addSplitMsgArr.shift();
  const addSplitMsg = addSplitMsgArr.join(delim);
  const resplit = addSplitMsg.split(',');
  if (resplit.length > 1) {
    for (let k = 0; k < resplit.length; k++) {
      addPlayers.push({ name: _.trim(resplit[k]), champ: '', role: '' });
    }
  } else {
    const spaceSplit = addSplitMsg.split(' ');
    for (let k = 0; k < spaceSplit.length; k++) {
      addPlayers.push({ name: _.trim(spaceSplit[k]), champ: '', role: '' });
    }
  }
};

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
});


client.on('message', (msg) => {
  let numTeams = 2;
  const splitMsg = msg.content.split(' ');
  if (splitMsg[0] !== '!wheel') {
    // end early if not calling the wheel
    return;
  }


  const curVoiceChannelID = msg.member.voiceChannelID;
  if (curVoiceChannelID === undefined || curVoiceChannelID === null) {
    msg.reply('You must be in a voice channel to use the wheel!');
    return;
  }

  const flags = {
    role: { silly: false, active: false },
    champ: { silly: false, active: false },
    solo: false,
  };

  let sillyMode = false;
  let endMode = false;
  const addPlayers = [];
  const errFlags = [];
  for (let i = 1; i < splitMsg.length; i++) {
    switch (splitMsg[i]) {
      case '?':
      case '/?':
      case '-?':
      case '--help':
      case 'help':
      case 'info':
        msg.channel.send(`🙂🎡🙂\nI can help you generate two (or one) LoL teams out of the people in this voice channel.\n\n\
Usage: \`!wheel [stupid] [champ] [role] [solo] [add] <extra players>\`\n\n
 - \`champ\` flag will also assign champions to play\n
 - \`role\` flag will also assign roles to play\n
 - Adding the \`stupid\` modifier before either or both of these will make the results (((**more better**)))\n
 - \`solo\` flag will only generate one team\n
 - You may list players (seperated with space or comma) after the \`add\` key to add additional players not currently in the call to the wheel.`);
        return;
      case 's':
      case 'solo':
        flags.solo = true;
        numTeams = 1;
        break;
      case 'stupid':
        sillyMode = true;
        break;
      case 'sr':
      case 'stupidrole':
      case 'stupidroles':
        sillyMode = true;
      // fallthrough
      case 'r':
      case 'role':
      case 'roles':
        flags.role.active = true;
        flags.role.silly = sillyMode;
        sillyMode = false;
        break;
      case 'sc':
      case 'stupidchamp':
      case 'stupidchamps':
        sillyMode = true;
      // fallthrough
      case 'c':
      case 'champ':
      case 'champs':
        flags.champ.active = true;
        flags.champ.silly = sillyMode;
        sillyMode = false;
        break;
      case 'a':
      case '+':
      case 'add':
        endMode = true;
        getPlayers(msg, addPlayers, splitMsg[i]);
        break;
      default:
        errFlags.push(splitMsg[i]);
    }
    if (endMode) {
      break;
    }
  }

  const p = [...addPlayers];
  // remove extraneous players left by double spaces etc
  _.remove(p, player => player.name === '');
  let unbalancedTeams = '';
  // build list of users
  const curVoiceChannel = msg.channel.guild.channels.get(curVoiceChannelID);
  curVoiceChannel.members.forEach((member) => {
    if (member.user.bot) {
      return;
    }
    p.push({
      name: member.user.username,
      champ: '',
      role: '',
    });
  });

  if (p.length > (5 * numTeams)) {
    if (flags.role.active) {
      msg.channel.send(`Can't have roles assigned for more than ${5 * numTeams} people you berk`);
      flags.role.active = false;
    }
  }
  const teams = [[], []];
  let team = 0;
  while (p.length) {
    const r = Math.floor(Math.random() * p.length);
    const name = p.splice(r, 1)[0];
    teams[team].push(name);
    team = (team + 1) % numTeams;
  }
  if (team && !flags.solo) {
    unbalancedTeams = 'LOL THESE TEAMS ARE UNBALANCED LOL FUCK\n\n';
  }

  // apply roles
  if (flags.role.active) {
    for (let t = 0; t < numTeams; t++) {
      const tempRoles = roles.slice();
      teams[t].forEach((player) => {
        const r = Math.floor(Math.random() * tempRoles.length);
        if (flags.role.silly) {
          player.role = tempRoles[r];
        } else {
          [player.role] = tempRoles.splice(r, 1);
        }
      });
    }
  }

  // apply champs
  const playedChamps = {};
  if (flags.champ.active) {
    for (let t = 0; t < numTeams; t++) {
      teams[t].forEach((player) => {
        // build list of available champs
        const availableChamps = [];
        const availableRoles = roleClasses[player.role];
        _.forOwn(allAvailableChamps, (champ, name) => {
          if (playedChamps[name] !== undefined) {
            return;
          }
          if (!flags.role.active || flags.champ.silly) {
            availableChamps.push(champ);
          }

          if (_.intersection(champ.tags, availableRoles).length) {
            availableChamps.push(champ);
          }
        });
        const champ = availableChamps[Math.floor(Math.random() * availableChamps.length)];
        player.champ = champ.name;
        playedChamps[champ.name] = champ;
      });
    }
  }

  const table = [flags.solo ? ['Fuckers'] : ['Team Eggs', 'Team Beans']];

  for (let i = 0; i < teams[0].length; i++) {
    table.push(flags.solo
      ? [
        displayPlayer(teams[0][i], flags),
      ]
      : [
        displayPlayer(teams[0][i], flags),
        displayPlayer(teams[1][i], flags),
      ]);
  }
  // generate output
  const tableOutput = `${unbalancedTeams}\`\`\`${markdownTable(table)}\`\`\``;
  msg.channel.send(tableOutput);
  if (errFlags.length) {
    msg.channel.send(`The following flags were not recognised, you dense motherfucker (use \`!wheel help\`): \`${errFlags.join('`,`')}\`\n\n`);
  }
});

client.login(token);
console.log(client);

const getNewAvatar = function getNewAvatar() {
  const start = Math.floor(Math.random() * 1234);
  GIS({
    queryStringAddition: `?start=${start}`,
    searchTerm: 'sad cats',
  }, (error, results) => {
    if (error) {
      return;
    }
    client.user.setAvatar(results[0].url).then(() => {
      console.log('suc');
    }).catch(() => {
      console.log('err');
    });
    console.log(results[0].url);
  });
  setTimeout(getNewAvatar, 24 * 3600 * 1000);
};


getNewAvatar();
